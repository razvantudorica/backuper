import Queue
import threading
import os
import boto3
import hashlib
import logging
import time
from botocore.exceptions import ClientError
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

from config import config


def get_file_list(folders):
    file_list = []

    for rootDir in folders:
        for dirName, subdirList, fileList in os.walk(rootDir):
            for fname in fileList:
                file_list.append(dirName + '/' + fname)

    return file_list


class S3Uploader():

    def __init__(self, s3client, s3resource):
        self._client = s3client
        self._resource = s3resource

    def upload(self, file_path):
        response = None

        file_key = file_path
        if file_path[0] == '/':
            file_key = file_path[1:]

        local_md5 = hashlib.md5(open(file_path, 'rb').read()).hexdigest()

        if not self._should_upload(file_key, local_md5):
            logging.info("Skip {0}".format(file_path))
            return response

        logging.info("Upload {0}".format(file_path))
        data = open(file_path, 'rb')
        response = self._resource.Bucket(bucket_name).put_object(Key=file_key, Body=data)

        return response

    def _should_upload(self, file_key, md5sum):
        logging.info("Checking file {0} in S3".format(file_key))
        try:
            response = self._client.head_object(Bucket=bucket_name, Key=file_key)
        except ClientError, e:
            response = e.response

        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            # file not in S3
            return True

        s3Etag = response['ETag'].strip('"')

        if md5sum != s3Etag:
            # not same md5, should upload
            return True

        # same etag
        return False


class Watcher(FileSystemEventHandler):

    def __init__(self, uploader=None):
        super(Watcher, self).__init__()

        self._uploader = uploader

    def on_modified(self, event):
        super(Watcher, self).on_modified(event)

        if not event.is_directory:
            logging.info("File changed {0}".format(event.src_path))
            response = self._uploader.upload(event.src_path)
            logging.info(response)


def start_watch(folders, uploader):

    file_created_handler = Watcher(uploader)
    observer = Observer()
    threads = []
    for folder in folders:
        logging.info("Start to watch {0}".format(folder))

        observer.schedule(file_created_handler, folder, recursive=True)

        threads.append(observer)

    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        logging.info("Stop backuper")
        observer.stop()

    observer.join()


def sync_worker(queue, uploader):
    queue_full = True
    while queue_full:
        try:
            file_path = queue.get(False)
        except Queue.Empty:
            queue_full = False

        if queue:
            try:
                response = uploader.upload(file_path)
            except Exception, e:
                logging.error("Could not upload {0}".format(file_path))


def check_current_files(folders, uploader):
    worker_data = get_file_list(folders)

    q = Queue.Queue()
    for file_path in worker_data:
        q.put(file_path)

    #create as many threads as you want
    thread_count = 5
    for i in range(thread_count):
        t = threading.Thread(target=sync_worker, args=(q, uploader))
        t.start()

# Start Code
if __name__ == '__main__':
    logging.info("Backuper started...")

    folders_to_watch = config['folders']
    bucket_name = config['bucket']
    client = boto3.client('s3')
    resource = boto3.resource('s3')

    s3uploader = S3Uploader(client, resource)

    check_current_files(folders_to_watch, s3uploader)

    start_watch(folders_to_watch, s3uploader)


