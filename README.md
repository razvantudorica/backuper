A small script used to backup (upload) the files from the specified directories in S3, keeping the same tree structure.

### Setup ###

* create an IAM user
* get credentials
* write the AWS credentials in ~/.aws/credentials:

```
#!bash

[default]
aws_access_key_id = 
aws_secret_access_key = 
```

* Attache a Policy in AWS console for this user:  AmazonS3FullAccess

* In folders.txt put the folder you want to backup in S3, one folder per row.
* In config.py replace the name of the bucket.
* Run: 

```
#!bash

sudo pip install -r requirements.txt
```


You can setup a cronjob to run backuper.py or run it manually.

backuper2.py
* it can run as a daemon and it watches for changes in the specified folders.
* it needs restart if new folder is added in folders.txt

## cli multi

To upload multiple files at once use cli_multi.py

Eg:

```
python cli_multi.py --folder /home/razvan/Documents/pics --s3path foder1/folder2/28aprilie/
```
