config = {
	'log_file': 'cli_multi.log',
	'number_of_threads': 10,
	'bucket': 'utgal',
	'region': 'eu-west-1',
	'profile': 'backuper',
}
