import boto3
import hashlib
from botocore.exceptions import ClientError

file_path = '/home/razvan/tmp/f2/f21/f211/bx.txt'

local_md5 = hashlib.md5(open(file_path, 'rb').read()).hexdigest()

bucket_name = 'utgaltest1'

s3 = boto3.client('s3')

try:
    response = s3.head_object(Bucket=bucket_name, Key=file_path[1:])
except ClientError, e:
    response = e.response

if response['ResponseMetadata']['HTTPStatusCode'] != 200:
    print "File not found in S3"
    exit(0)

s3Etag = response['ETag'].strip('"')

if s3Etag == local_md5:
    print "Same file", s3Etag, local_md5
else:
    print "Different file", s3Etag, local_md5
