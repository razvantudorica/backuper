import logging

# @todo, a real config file

folders = list()
folderFile = open('folders.txt', 'r')
for line in folderFile:
    if line.startswith(('#', ' ', '\n')):
        continue
    folders.append(line.strip('\n'))

config = {
    'bucket': 'dropbox.razvantudorica.net',
    'aws_profile': 'owncloud',

    'folders': folders,

    # technical side
    'number_of_threads': 10,
}

logging.basicConfig(filename='backuper.log',
                    level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')
