import sys
import threading
import os
import boto3
import hashlib
import logging
from botocore.exceptions import ClientError
import time
from config import config

is_py2 = sys.version[0] == '2'
if is_py2:
    import Queue
else:
    import queue as Queue


def get_file_list(folders):
    filelist = []

    for rootDir in folders:
        for dirName, subdirList, fileList in os.walk(rootDir):
            # print('Found directory: %s' % dirName)
            for fname in fileList:
                filelist.append(dirName + '/' + fname)

    return filelist


def should_upload(file_key, md5sum):
    try:
        response = client.head_object(Bucket=bucket_name, Key=file_key)
    except ClientError as e:
        response = e.response

    if response['ResponseMetadata']['HTTPStatusCode'] != 200:
        # file not in S3, should upload
        return True

    s3Etag = response['ETag'].strip('"')

    if md5sum != s3Etag:
        # not same md5, should upload
        return True

    # same etag, do not upload
    return False


def upload(file_path):
    response = None

    file_key = file_path
    if file_path[0] == '/':
        file_key = file_path[1:]
   
    try:
        local_md5 = hashlib.md5(open(file_path, 'rb').read()).hexdigest()
    except FileNotFoundError:
        logging.error("File {} not found".format(file_path))
        return None

    if not should_upload(file_key, local_md5):
        logging.info("Skip {0}".format(file_path))
        return response

    units = 'MB'
    size = round(os.path.getsize(file_path) / 1024 * 1.0 / 1024, 2)
    if size > 0:
        size = round(os.path.getsize(file_path) / 1024 * 1.0, 2)
        units = 'KB'

    logging.info("Upload {0} ... {1} {2}".format(file_path, size, units))

    data = open(file_path, 'rb')
    response = s3.Bucket(bucket_name).put_object(Key=file_key, Body=data)

    return response


def download():
    # todo
    key = boto3.resource('s3').Object('fooo', 'docker/my-image.tar.gz').get()
    with open('/tmp/my-image.tar.gz', 'w') as f:
        chunk = key['Body'].read(1024*8)
        while chunk:
            f.write(chunk)


def worker(queue):
    queue_full = True
    while queue_full:
        try:
            fpath = queue.get(False)
            upload(fpath)
        except Queue.Empty:
            queue_full = False


# Start Code
if __name__ == '__main__':

    logging.info("Backuper started. Getting file list...")
    folders = config['folders']
    bucket_name = config['bucket']

    worker_data = get_file_list(folders)

    session = boto3.Session(profile_name=config['aws_profile'])

    s3 = session.resource('s3')
    client = session.client('s3')

    #load up a queue with your data, this will handle locking
    q = Queue.Queue()
    for file_path in worker_data:
        q.put(file_path)

    #create as many threads as you want
    thread_count = config.get('number_of_threads', 5)
    logging.info("Sync started with {} threads".format(thread_count))

    for i in range(thread_count):
        t = threading.Thread(target=worker, args = (q,))
        t.start()
