#!/bin/bash

cp backuper.py /usr/bin/backuper
update-rc.d -f backuperd remove

cp scripts/backuperd.sh /etc/init.d/backuperd
chmod 755 /etc/init.d/backuperd

update-rc.d backuperd defaults