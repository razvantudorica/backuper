import logging
import sys
import os
import signal
import hashlib
from time import sleep
from multiprocessing import Process, Queue, active_children
from botocore.exceptions import ClientError
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import boto3

from config import config


class Watcher(FileSystemEventHandler):

    def __init__(self, uploader):
        super(Watcher, self).__init__()
        self._uploader = uploader

    def on_modified(self, event):
        super(Watcher, self).on_modified(event)

        if not event.is_directory:
            logging.info("File changed {0}".format(event.src_path))
            response = self._uploader.upload(event.src_path)
            logging.info(response)


class S3Uploader():

    def __init__(self, s3client, s3resource):
        self._client = s3client
        self._resource = s3resource

    def upload(self, file_path):
        response = None

        file_key = file_path
        if file_path[0] == '/':
            file_key = file_path[1:]

        local_md5 = hashlib.md5(open(file_path, 'rb').read()).hexdigest()

        if not self._should_upload(file_key, local_md5):
            logging.info("Skip {0}".format(file_path))
            return response

        logging.info("Upload {0}".format(file_path))
        data = open(file_path, 'rb')
        response = self._resource.Bucket(bucket_name).put_object(Key=file_key, Body=data)

        return response

    def _should_upload(self, file_key, md5sum):
        logging.info("Checking file {0} in S3".format(file_key))
        try:
            response = self._client.head_object(Bucket=bucket_name, Key=file_key)
        except ClientError as e:
            response = e.response

        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            # file not in S3
            return True

        s3Etag = response['ETag'].strip('"')

        if md5sum != s3Etag:
            # not same md5, should upload
            return True

        # same etag
        return False


def watcher(work_queue, done_queue):
    for folder in iter(work_queue.get, 'STOP'):
        logging.info("New process {0}. Watch folder {1}".format(os.getpid(), folder))
        session = boto3.Session(profile_name=config['aws_profile'])
        client = session.client('s3')
        resource = session.resource('s3')

        uploader = S3Uploader(client, resource)
        file_created_handler = Watcher(uploader)

        observer = Observer()
        observer.schedule(file_created_handler, folder, recursive=True)
        observer.start()

        try:
            while True:
                sleep(1)
        except KeyboardInterrupt:
            logging.info("Stop backuper ID {0}".format(os.getpid()))
            observer.stop()
            sys.exit()
        except Exception as e:
            print ("Exception: {}".format(e))

        observer.join()


def kill_function(sig_num, stack):
    logging.info("Kill signal received. Shutting down ALL!")
    for kid in active_children():
        kid.terminate()

    sys.exit()


def confirm_folders(folders):
    folders_to_watch = []
    for folder in folders:
        if os.path.isdir(folder):
            folders_to_watch.append(folder)

    return folders_to_watch


# Start Code
if __name__ == '__main__':

    folders_to_watch = confirm_folders(config['folders'])
    if len(folders_to_watch) == 0:
        logging.info("Nothing to backup. Exit!")
        sys.exit()

    signal.signal(signal.SIGUSR1, kill_function)

    logging.info("Backuper3 started with PID {0}...".format(os.getpid()))
    bucket_name = config['bucket']

    work_queue = Queue()
    done_queue = Queue()
    processes = []

    for f in folders_to_watch:
        work_queue.put(f)

    for w in range(len(folders_to_watch)):
        p = Process(target=watcher, args=(work_queue, done_queue))
        p.start()
        processes.append(p)
        work_queue.put('STOP')

    for p in processes:
        try:
            p.join()
        except:
            logging.info("Stop parent backuper ID {0}".format(os.getpid()))
            p.terminate()
            sys.exit()

    done_queue.put('STOP')

    for status in iter(done_queue.get, 'STOP'):
        logging.info("Status {0}".format(status))


