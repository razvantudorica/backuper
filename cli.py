import os
import argparse
import sys
import subprocess

BUCKET = 'dropbox.razvantudorica.net'
REGION = 'eu-central-1'
PROFILE = 'owncloud'

def get_file_list(folders):
	"""
	:type folders: list
	"""
	filelist = []

	for rootDir in folders:
		for dirName, subdirList, fileList in os.walk(rootDir):
		# print('Found directory: %s' % dirName)
			for fname in fileList:
				path = os.path.join(dirName, fname)
				filelist.append(path)

	return filelist

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--folder', help='Folder to backup', required=True)
	parser.add_argument('--s3path', help='The path in S3 where to be written. Eg: /backup/')
	args = parser.parse_args()

	folder = args.folder
	if os.path.isdir(folder):
		if folder[-1] != os.sep:
			folder += os.sep

	print(folder)

	s3path = args.s3path
	if not s3path:
		# TODO
		s3path = folder.replace(folder, '')
	
	filelist = get_file_list((folder,))

	for filename in filelist:
		# TODO = ensure s3path has / at the end
		# print (filename)
		# if filename[0] == os.sep:
		# 	filename = filename[1:]

		s3file = s3path + filename.replace(folder, '')

		# print([filename, s3file])	

		# cmd = 'aws s3 cp "{filename}" "s3://{bucket}/{s3path}" --region={region} --profile={profile}'.format(
		# 	filename=filename, bucket=BUCKET, s3path=s3file, region=REGION, profile=PROFILE)

		# print (cmd)

		try:
			res = subprocess.check_output(['aws', 's3', 'ls', 's3://{bucket}/{s3path}'.format(bucket=BUCKET, s3path=s3file), '--region={region}'.format(region=REGION), '--profile={profile}'.format(profile=PROFILE)])
			print ("SKIP: {}".format(filename))
		except:
			print ("UPLOAD: {} -> {}".format(filename, s3file))

			subprocess.check_output(['aws', 's3', 'cp', '{filename}'.format(filename=filename), 's3://{bucket}/{s3path}'.format(bucket=BUCKET, s3path=s3file), 
			'--region={region}'.format(region=REGION), '--profile={profile}'.format(profile=PROFILE),])

		# print(res)

		# subprocess.check_output(['aws', 's3', 'cp', '{filename}'.format(filename=filename), 's3://{bucket}/{s3path}'.format(bucket=BUCKET, s3path=s3file), 
		# 	'--region={region}'.format(region=REGION), '--profile={profile}'.format(profile=PROFILE),])

		